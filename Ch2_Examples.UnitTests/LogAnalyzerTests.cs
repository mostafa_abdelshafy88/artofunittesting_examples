﻿using Ch2_Example;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch2_Examples.UnitTests
{
    [TestFixture]
    public class LogAnalyzerTests
    {
        public int i = 0;

        //****

        [SetUp]
        public void Setup()
        {
            i++;
            Debug.WriteLine("Before Any Test, Only Once, {0}", i);
        }

        [Category("Cat1")]
        [Test]
        public void IsValidLogFileName_BadExtension_ReturnsFalse()
        {
            //Arrange
            LogAnalyzer analyzer = new LogAnalyzer();
            //Act
            bool result = analyzer.IsValidLogFileName("filewithbadextension.foo");
            //Assert
            Debug.WriteLine(i);
            Assert.False(result);
        }

        [Category("Cat2")]
        [Test]
        public void IsValidLogFileName_GoodExtensionLowercase_ReturnsTrue()
        {
            LogAnalyzer analyzer = new LogAnalyzer();

            bool result = analyzer.IsValidLogFileName("filewithgoodextension.slf");
            Debug.WriteLine(i);
            Assert.True(result);
        }

        [Category("Cat1")]
        [Test]
        public void IsValidLogFileName_GoodExtensionUppercase_ReturnsTrue()
        {
            LogAnalyzer analyzer = new LogAnalyzer();

            bool result = analyzer.IsValidLogFileName("filewithgoodextension.SLF");
            Debug.WriteLine(i);
            Assert.True(result);
        }

        // this is a refactoring of the previous two tests
        [TestCase("filewithgoodextension.SLF")]
        [TestCase("filewithgoodextension.slf")]
        public void IsValidLogFileName_ValidExtensions_ReturnsTrue(string file)
        {
            LogAnalyzer analyzer = new LogAnalyzer();

            bool result = analyzer.IsValidLogFileName(file);

            Assert.True(result);
        }

        // this is a refactoring of all the "regular" tests
        [TestCase("filewithgoodextension.SLF", true)]
        [TestCase("filewithgoodextension.slf", true)]
        [TestCase("filewithbadextension.foo", false)]
        public void IsValidLogFileName_VariousExtensions_ChecksThem(string file, bool expected)
        {
            LogAnalyzer analyzer = new LogAnalyzer();

            bool result = analyzer.IsValidLogFileName(file);

            Assert.AreEqual(expected, result);
        }

        [Category("Cat1")]
        [Test]
        public void IsValidLogFileName_EmptyFileName_ThrowException()
        {
            LogAnalyzer analyzer = new LogAnalyzer();

            var ex = Assert.Throws<ArgumentException>(() => analyzer.IsValidLogFileName(""));
            Assert.True(ex.Message.Contains("filename has to be provided"));
            Assert.IsTrue(ex is ArgumentException);
        }

        [Ignore("under maintenance")]
        [Category("Cat2")]
        //Elegant New Exception Testing
        [Test]
        public void IsValidLogfileName_FirstTrialEmptyFileName_ThrowsException()
        {
            //Arrange
            var analyzer = new LogAnalyzer();

            //Assert

            var ex = Assert.Catch<ArgumentException>(() => analyzer.IsValidLogFileName(""));
            StringAssert.Contains("filename has to be provided", ex.Message);
        }

        [Category("Cat2")]
        // [Ignore("temporary ignored till blah blah blah")]
        //More Elegant Exception Handling

        [Test]
        public void IsValidLogFileName_SecondTryEmptyFileName_ThrowException()
        {
            //Arrange
            LogAnalyzer analyzer = new LogAnalyzer();

            //Sort of An Act
            ActualValueDelegate<bool> delegateForResult = () => analyzer.IsValidLogFileName("");

            //Assert
            Assert.That(delegateForResult, Throws.TypeOf<ArgumentException>());
        }

        [Test]
        public void IsValidLogFileName_WhenCalled_ChangesWasLAstFileNameValid()
        {
            //Arrange
            LogAnalyzer la = new LogAnalyzer();
            //Act
            la.IsValidLogFileName("badname.foo");
            //Assert

            /*
             * testing the functionality of the  IsValidLogFileName method by
                asserting against code in a different location than the piece of code under test
             */
            Assert.False(la.WasLastFileNameValid);
        }

        [TestCase("badfile.foo", false)]
        [TestCase("goodfile.slf", true)]
        public void IsValidLogFileName_WhenCalled_ChangesWasLastFileNameValid(string file, bool expected)
        {
            LogAnalyzer la = new LogAnalyzer();

            la.IsValidLogFileName(file);

            Assert.AreEqual(expected, la.WasLastFileNameValid);
        }
    }
}