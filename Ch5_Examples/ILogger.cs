﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch5_Examples
{
    public interface ILogger
    {
        void LogError(string message);
    }
}