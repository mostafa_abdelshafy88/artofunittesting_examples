﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;

namespace Ch7_Examples.UnitTests
{
    [TestFixture]
    public class LogAnalyzerTests
    {
        [Test]
        public void Analyze_EmptyFile_ThrowsException()
        {
            LogAnalyzer la = new LogAnalyzer();
            la.Analyze("myEmptyFile.txt");
            //rest of test
        }

        [TearDown]
        public void teardown()
        {
            //need to reset a static resource between tests
            LoggingFacility.Logger = null;
        }
    }

    [TestFixture]
    public class ConfigurationManagerTests
    {
        public void Analyze_EmptyFile_ThrowsException()
        {
            ConfigurationManager cm = new ConfigurationManager();
            bool configured = cm.IsConfigured("something");

            //rest of the test
        }

        [TearDown]
        public void teardown()
        {
            //need to reset a static resource between tests
            LoggingFacility.Logger = null;
        }
    }
}