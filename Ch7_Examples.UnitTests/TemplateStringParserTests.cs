﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch7_Examples.UnitTests
{
    [TestFixture]
    public abstract class TemplateStringParserTests
    {
        public abstract void TestGetStringVersionFromHeader_SingleDigit_Found();

        public abstract void TestGetStringVersionFromHeader_WithMinorVersion_Found();

        public abstract void TestGetStringVersionFromHeader_WithRevision_Found();
    }

    public class XmlStringParserTests : TemplateStringParserTests
    {
        protected IStringParser GetParser(string input)
        {
            return new XMLStringParser(input);
        }

        [Ignore("Not Fully Implemented")]
        [Test]
        public override void TestGetStringVersionFromHeader_SingleDigit_Found()
        {
            IStringParser parser = GetParser("<Header>1</Header>");

            string versionFromHeader = parser.GetStringVersionFromHeader();
            Assert.AreEqual("1", versionFromHeader);
        }

        [Ignore("Not Fully Implemented")]
        [Test]
        public override void TestGetStringVersionFromHeader_WithMinorVersion_Found()
        {
            IStringParser parser = GetParser("<Header>1.1</Header>");

            string versionFromHeader = parser.GetStringVersionFromHeader();
            Assert.AreEqual("1.1", versionFromHeader);
        }

        [Ignore("Not Fully Implemented")]
        [Test]
        public override void TestGetStringVersionFromHeader_WithRevision_Found()
        {
            IStringParser parser = GetParser("<Header>1.1.1</Header>");

            string versionFromHeader = parser.GetStringVersionFromHeader();
            Assert.AreEqual("1.1.1", versionFromHeader);
        }
    }

    public class StdStringParserTests : TemplateStringParserTests
    {
        protected IStringParser GetParser(string input)
        {
            return new XMLStringParser(input);
        }

        [Ignore("Not Fully Implemented")]
        [Test]
        public override void TestGetStringVersionFromHeader_SingleDigit_Found()
        {
            IStringParser parser = GetParser("1");

            string versionFromHeader = parser.GetStringVersionFromHeader();
            Assert.AreEqual("1", versionFromHeader);
        }

        [Ignore("Not Fully Implemented")]
        [Test]
        public override void TestGetStringVersionFromHeader_WithMinorVersion_Found()
        {
            IStringParser parser = GetParser("1.1");

            string versionFromHeader = parser.GetStringVersionFromHeader();
            Assert.AreEqual("1.1", versionFromHeader);
        }

        [Ignore("Not Fully Implemented")]
        [Test]
        public override void TestGetStringVersionFromHeader_WithRevision_Found()
        {
            IStringParser parser = GetParser("1.1.1");

            string versionFromHeader = parser.GetStringVersionFromHeader();
            Assert.AreEqual("1.1.1", versionFromHeader);
        }
    }
}