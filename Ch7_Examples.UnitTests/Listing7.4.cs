﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;

namespace Ch7_Examples.UnitTests
{
    [TestFixture]
    public class BaseTestsClass
    {
        public ILogger FakeTheLogger()
        {
            LoggingFacility.Logger = Substitute.For<ILogger>();
            return LoggingFacility.Logger;
        }

        [TearDown]
        public void TearDown()
        {
            LoggingFacility.Logger = null;
        }
    }

    [TestFixture]
    public class ConfigurationMangerTests : BaseTestsClass
    {
        [Test]
        public void Analyze_EmptyFile_ThrowsException()
        {
            FakeTheLogger();
            ConfigurationManager cm = new ConfigurationManager();
            bool configured = cm.IsConfigured("something");

            //rest of the test
        }
    }
}