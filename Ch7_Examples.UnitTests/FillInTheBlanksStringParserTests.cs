﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch7_Examples.UnitTests
{
    [TestFixture]
    public abstract class FillInTheBlanksStringParserTests
    {
        protected abstract IStringParser GetParser(string input);

        protected abstract string HeaderVersion_SingleDigit { get; }
        protected abstract string HeaderVersion_WithRevision { get; }
        protected abstract string HeaderVersion_WithMinorVersion { get; }

        public const string EXPECTED_SINGLE_DIGIT = "1";
        public const string EXPECTED_WITH_REVISION = "1.1.1";
        public const string EXPECTED_WITH_MINOREVISION = "1.1";

        // [Test]
        public void TestGetStringVersionFromHeader_SingleDigit_Found()
        {
            string input = HeaderVersion_SingleDigit;
            IStringParser parser = GetParser(input);

            string versionFromHeader = parser.GetStringVersionFromHeader();
            Assert.AreEqual(EXPECTED_SINGLE_DIGIT, versionFromHeader);
        }

        //[Test]
        public void TestGetStringVersionFromHEader_WithMinorVersion_Found()
        {
            string input = HeaderVersion_WithMinorVersion;
            IStringParser parser = GetParser(input);

            string versionFromHeader = parser.GetStringVersionFromHeader();
            Assert.AreEqual(EXPECTED_WITH_MINOREVISION, versionFromHeader);
        }

        // [Test]
        public void TestGetStringVersionFromHEader_WithRevision_Found()
        {
            string input = HeaderVersion_WithRevision;
            IStringParser parser = GetParser(input);
            string versionFromHeader = parser.GetStringVersionFromHeader();
            Assert.AreEqual(EXPECTED_WITH_REVISION, versionFromHeader);
        }
    }

    public class StandardStringParserTests : FillInTheBlanksStringParserTests
    {
        protected override string HeaderVersion_SingleDigit
        {
            get
            {
                return string.Format("header\tversion={0}\t\n", EXPECTED_SINGLE_DIGIT);
            }
        }

        protected override string HeaderVersion_WithMinorVersion
        {
            get
            {
                return string.Format("header\tversion={0}\t\n", EXPECTED_WITH_MINOREVISION);
            }
        }

        protected override string HeaderVersion_WithRevision
        {
            get
            {
                return string.Format("header\tversion={0}\t\n", EXPECTED_WITH_REVISION);
            }
        }

        protected override IStringParser GetParser(string input)
        {
            return new StandardStringParser(input);
        }
    }
}