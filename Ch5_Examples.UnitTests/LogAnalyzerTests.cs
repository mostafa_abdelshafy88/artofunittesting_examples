﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch5_Examples.UnitTests
{
    [TestFixture]
    public class LogAnalyzerTests
    {
        [Test]
        public void Analyze_TooShortFilename_CallLogger()
        {
            //Arrange
            FakeLogger logger = new FakeLogger();
            LogAnalyzer analyzer = new LogAnalyzer(logger);

            //Act
            analyzer.MinNameLength = 6;
            analyzer.Analyze("a.txt");

            //Assert
            StringAssert.Contains("too short", logger.LastError);
        }

        [Test]
        public void Analyze_TooShortFilename_CallLogger_UsingNsub()
        {
            //Arrange
            ILogger logger = Substitute.For<ILogger>();
            LogAnalyzer analyzer = new LogAnalyzer(logger);

            //Act
            analyzer.MinNameLength = 6;
            analyzer.Analyze("a.txt");

            //Assert
            //Actually we are asking if the method named Log Error got called
            //And when got called was having this exact paramter
            logger.Received().LogError("Filename too short: a.txt");
        }

        [Test]
        public void Returns_ByDefault_WorksForHArdCodedArgument()
        {
            IFileNameRules fakeRules = Substitute.For<IFileNameRules>();
            fakeRules.IsValidLogFileName(Arg.Any<string>()).Returns(true);
            Assert.IsTrue(fakeRules.IsValidLogFileName("strict.txt"));
        }

        [Test]
        public void Returns_ArgAny_Throws()
        {
            IFileNameRules fakeRules = Substitute.For<IFileNameRules>();
            fakeRules.When(x => x.IsValidLogFileName(Arg.Any<string>()))
                .Do(context => { throw new Exception("fake Exception"); });
            Assert.Throws<Exception>(() =>
            fakeRules.IsValidLogFileName("anything"));
        }

        [Test]
        public void Analyze_LoggerThrows_CallsWebService()
        {
            FakeWebService mockWebService = new FakeWebService();

            FakeLogger2 stubLogger = new FakeLogger2();
            stubLogger.WillThrow = new Exception("fake exception");

            LogAnalyzer2 analyzer2 = new LogAnalyzer2(stubLogger, mockWebService);
            analyzer2.MinNameLength = 8;

            string tooShortFileName = "abc.ext";
            analyzer2.Analyze(tooShortFileName);

            Assert.That(mockWebService.MessageToWebService,
                            Is.SupersetOf("fake exception"));
        }

        [Test]
        public void Analyze_LogggerThrows_CallsWebService()
        {
            var webServiceMock = Substitute.For<IWebService>();
            var loggerStub = Substitute.For<ILogger>();
            loggerStub.When(x => x.LogError(Arg.Any<string>()))
                .Do(info => { throw new Exception("fake Exception"); });

            var analyzer = new LogAnalyzer2(loggerStub, webServiceMock);
            analyzer.MinNameLength = 10;
            analyzer.Analyze("Short.txt");

            webServiceMock.Received()
                .Write(Arg.Is<string>(s => s.Contains("fake Exception")));
        }

        [Test]
        public void Analyze_LoggerThrows_CallsWebServiceWithNSubObject()
        {
            var mockWebService = Substitute.For<IWebService>();
            var stubLogger = Substitute.For<ILogger>();
            stubLogger.When(x => x.LogError(Arg.Any<string>()))
                .Do(info => { throw new Exception("fake Exception"); });
            var analyzer = new LogAnalyzer3(stubLogger, mockWebService);
            analyzer.MinNameLength = 10;
            analyzer.Analyze("Short.txt");
            mockWebService.Received()
                .Write(Arg.Is<ErrorInfo>(info => info.Severity == 1000 && info.Message.Contains("fake Exception")));
        }
    }

    public class FakeWebService : IWebService
    {
        public string MessageToWebService;

        public void Write(string message)
        {
            MessageToWebService = message;
        }

        public void Write(ErrorInfo message)
        {
        }
    }

    public class FakeLogger2 : ILogger
    {
        public Exception WillThrow = null;
        public string LoggerGotMessage = null;

        public void LogError(string message)
        {
            LoggerGotMessage = message;
            if (WillThrow != null)
            {
                throw WillThrow;
            }
        }
    }
}