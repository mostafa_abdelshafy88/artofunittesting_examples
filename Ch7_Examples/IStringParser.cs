﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch7_Examples
{
    public interface IStringParser
    {
        string StringToParse { get; }

        bool HasCorrectHeader();

        string GetStringVersionFromHeader();
    }
}