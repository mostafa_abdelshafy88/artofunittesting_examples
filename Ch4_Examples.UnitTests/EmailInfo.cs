﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch4_Examples.UnitTests
{
    public class EmailInfo
    {
        public string To { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as EmailInfo);
        }

        public bool Equals(EmailInfo emailInfo)
        {
            if (Object.ReferenceEquals(emailInfo, null))
            {
                return false;
            }
            if (Object.ReferenceEquals(this, emailInfo))
            {
                return true;
            }
            if (this.GetType() != emailInfo.GetType())
            {
                return false;
            }
            return ((this.To.Equals(emailInfo.To, StringComparison.OrdinalIgnoreCase))
                && (this.Body.Equals(emailInfo.Body, StringComparison.OrdinalIgnoreCase))
                && (this.Subject.Equals(emailInfo.Subject, StringComparison.OrdinalIgnoreCase)));
        }
    }
}