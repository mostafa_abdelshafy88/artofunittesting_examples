﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch4_Examples.UnitTests
{
    public class FakeEmailService2 : IEmailService
    {
        public EmailInfo EmailInfo { get; set; }

        public void SendEmail(string to, string subject, string body)
        {
            EmailInfo = new EmailInfo();
            EmailInfo.To = to;
            EmailInfo.Body = body;
            EmailInfo.Subject = subject;
        }
    }
}