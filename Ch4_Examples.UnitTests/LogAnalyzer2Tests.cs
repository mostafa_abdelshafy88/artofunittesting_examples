﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch4_Examples.UnitTests
{
    [TestFixture]
    public class LogAnalyzer2Tests
    {
        [Test]
        public void Analyze_WebServiceThrows_SendEmail()
        {
            //Arrange
            FakeWebService2 stubService = new FakeWebService2();
            stubService.ToThrow = new Exception("fake exception");
            FakeEmailService mockEmail = new FakeEmailService();
            LogAnalyzer2 log = new LogAnalyzer2(stubService, mockEmail);
            string tooShortFilename = "abc.ext";

            //Act
            log.Analyze(tooShortFilename);

            //Assert
            StringAssert.Contains("someone@somweher.com", mockEmail.To);
            StringAssert.Contains("fake exception", mockEmail.Body);
            StringAssert.Contains("can't log", mockEmail.Subject);
        }

        //Same As the one Above but with only one Assert Statement using the Email Info Class

        [Test]
        public void Analyze_WebServiceThrows_SendEmailElaborate()
        {
            //Arrange
            FakeWebService2 stubService = new FakeWebService2();
            stubService.ToThrow = new Exception("fake exception");
            FakeEmailService2 mockEmail = new FakeEmailService2();
            LogAnalyzer2 log = new LogAnalyzer2(stubService, mockEmail);
            string tooShortFilename = "abc.ext";

            //Act
            log.Analyze(tooShortFilename);

            //Assert
            var expectedMailInfoSend = new EmailInfo();
            expectedMailInfoSend.To = "someone@somweher.com";
            expectedMailInfoSend.Body = "fake exception";
            expectedMailInfoSend.Subject = "can't log";

            Assert.AreEqual(expectedMailInfoSend, mockEmail.EmailInfo);
        }
    }
}