﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter3_Examples
{
    public interface IExtensionManager
    {
        bool IsValid(string fileName);
    }
}