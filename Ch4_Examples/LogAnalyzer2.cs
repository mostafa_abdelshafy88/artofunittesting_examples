﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch4_Examples
{
    public class LogAnalyzer2
    {
        public IWebService Service { get; set; }
        public IEmailService Email { get; set; }

        public LogAnalyzer2(IWebService service, IEmailService email)
        {
            Service = service;
            Email = email;
        }

        public void Analyze(string fileName)
        {
            if (fileName.Length < 8)
            {
                try
                {
                    Service.LogError("Filename is too short: " + fileName);
                }
                catch (Exception e)
                {
                    Email.SendEmail("someone@somweher.com", "can't log", e.Message);
                }
            }
        }
    }
}