﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch4_Examples
{
    public class LogAnalyzer
    {
        private IWebService _service;

        public LogAnalyzer(IWebService service)
        {
            this._service = service;
        }

        public void Analyze(string fileName)
        {
            if (fileName.Length < 8)
            {
                _service.LogError("Filename is too short: " + fileName);
            }
        }
    }
}