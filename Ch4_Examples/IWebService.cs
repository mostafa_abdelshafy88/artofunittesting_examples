﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch4_Examples
{
    public interface IWebService
    {
        void LogError(string message);
    }
}